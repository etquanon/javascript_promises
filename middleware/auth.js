const isAuth = (req, res, next) => {
    if (req.query.token === 'qwerty') {
        next();
    }

    res.send(403, 'Upsie!');
};

module.exports = isAuth;