const axios = require('axios');

const getPhotos = async () => {
    let promises = [];

    try {

        for (let i = 0; i < 50; i++) {
            // let r = await axios.get('https://jsonplaceholder.typicode.com/photos');
            promises[i] = axios.get('https://jsonplaceholder.typicode.com/photos');
        }

        result = await Promise.all(promises);      
    } catch (error) {
        throw new Error(error);
    }

    return result.map(item => item.data);
};

const getPhotoById = (id) => {
    axios.get('https://jsonplaceholder.typicode.com/photos', {
        id
    }).then((response) => {
        return response.data;
    });
};

module.exports = {
    getPhotos,
    getPhotoById
}

