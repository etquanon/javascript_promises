const express = require('express');
const app = express();
const port = 3000;

//local imports
const authMiddleware = require('./middleware/auth');
const photosModel = require('./models/photos');

app.get('/', (req, res) => res.send('Hello Worldzzzz!'));

app.get('/photos', (req, res, next) => {
    photosModel.getPhotos().then((response) => {
        res.send(response);
    });
});

app.get('/photos', (req, res, next) => {
    photosModel.getPhotos(req.query.id).then((response) => {
        res.send(response);
    });
});

app.use('/robert', authMiddleware, (req, res) => res.send('Hello Robert!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));