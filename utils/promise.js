// var data = [];
console.time();

var somePromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve([
      {
        id: 1,
        name: 'John'
      },
      {
        id: 2,
        name: 'Jimmy'
      }
    ]);
    // reject('Unable to fulfill promise');
  }, 2500);
});

somePromise.then((data) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data.map(item => {
        console.timeEnd();
        return {
          id: item.id * 2,
          name: 'Sir ' + item.name
        };
      }));
    }, 4000); 
  })
}, (errorMessage) => {
  console.log('Error: ', errorMessage);
}).then(data => {
  console.log(data);
})
.catch(error => {
  throw new Error("la naiba!");
});


