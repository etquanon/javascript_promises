// var getUser = (id, callback) => {
//   var user = {
//     id: id,
//     name: 'Vikram'
//   };

//   setTimeout(() => {
//     callback(user);
//   }, 3000);
// };

// getUser(31, (userObject) => {
//   console.log(userObject);
// });

var getUsers = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve([
      {
        id: 1,
        name: 'John'
      },
      {
        id: 2,
        name: 'Jimmy'
      }
    ]);
    // reject('Unable to fulfill promise');
  }, 2500);
});

var getProducts = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve([
      {
        id: 1,
        name: 'Televizor alb-negru'
      },
      {
        id: 2,
        name: 'Nutella'
      }
    ]);
    // reject('Unable to fulfill promise');
  }, 4000);
});

console.time();

// Promise.race([getUsers, getProducts]).then(data => {
//   console.log(data);

//   console.timeEnd();
// });

function resolveAfter2Seconds() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('resolved');
      // reject('resolved');
    }, 2000);
  });
}

function resolveAfter3Seconds() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('resolved');
      // reject('resolved');
    }, 3000);
  });
}

async function asyncCall() {
  console.time();
  console.log('calling');
  let result, r2;
  
  try {
    // result = await resolveAfter2Seconds();
    // r2 = await resolveAfter3Seconds();
    result = await Promise.all([resolveAfter2Seconds(), resolveAfter3Seconds()]);
  } catch (error) {
    throw new Error(error);
  }

  console.log(result);
  console.timeEnd();
}

asyncCall();
